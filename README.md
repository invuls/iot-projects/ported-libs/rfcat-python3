Its a clone of original project https://github.com/atlas0fd00m/rfcat ported to Windows OS and Python v3.

To install:

```python
python3 setup.py install
```

Also you need to use libusb0, not libusb1 (windows).

That's why you need to delete libusb1 DLL's from PATH and move /win32-driver/libusb0.dll to, for example, C:\Windows\System32\

